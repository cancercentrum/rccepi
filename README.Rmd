---
output:
  md_document:
    variant: markdown_github
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
```

NOTE!!! This package is currently under development! 
It is OK to test but there are no warranties of any kind so far :-)

# Install
```
devtools::install_bitbucket("cancercentrum/rccepi")
library(rccepi)
?incidence
?incidence.test
```

