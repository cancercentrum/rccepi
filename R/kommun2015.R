#' Add municipality code based on old LKF codes
#'
#' @param x data frame with cancer register data
#' @param year name of column indicating year of diagnosis
#' @param lkf name of column indicating lkf code
#'
#' @return sama data set with new column "kommun",
#' with the municipality code valid 2015 or later.
#' @export
kommun2015 <- function(x, year, lkf) {
  x <- dplyr::right_join(x, rccepi::kommun2015, c(year = year, lkf = lkf))
  x$kommun <- dplyr::coalesce(x$kommun2015_code, substr(x[[lkf]], 1, 4))
}