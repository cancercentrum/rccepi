# Data file generated and downloaded from:
# http://regina.scb.se/indelningar#
# Församling without filter
# File reopenned as ISO-latin1 and resaved as UTF-8
fs_all <-
  read.delim(
    "data-raw/Indelningar församlingar.txt",
    colClasses = "character"
  ) %>%
  as_tibble()

# Data file generated and downloaded from:
# http://regina.scb.se/indelningsandringar#
# All years and all changes
# File reopenned as ISO-latin1 and resaved as UTF-8
fs_changes <-
  read.delim(
    "data-raw/Indelningsändringar församlingar",
    colClasses = "character"
  )

# Check if any changes made not 1st of January
filter(fs_changes, !endsWith(Datum.ikraftträdande, "-01-01"))
# Cases exist but only for irrelevant changes (names not codes)

fs_changes <-
  fs_changes %>%
    as_tibble() %>%
    filter(Ändring != "Namnändrad (ej kodändrad)") %>%
    transmute(
      old_code = Gammal.kod,
      new_code = Ny.kod,
      old_name = Gammalt.namn,
      new_name = Nytt.namn,
      nm_chg   = new_name != old_name,
      when     = substr(Datum.ikraftträdande, 1, 4),
      what     = Ändring
    )

usethis::use_data(fs_changes)


# Har nu två olika dataset med samtliga församlingskoder som någonsin använts,
# samt när och vilka ändringar som skett.
# Bör på ngt sätt utifrån detta kunna återskapa den lista Håpkan visade.
