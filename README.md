<!-- README.md is generated from README.Rmd. Please edit that file -->
[![Travis-CI Build
Status](https://travis-ci.org/cancercentrum/rccepi%3E.svg?branch=master)](https://travis-ci.org/cancercentrum/rccepi)

NOTE!!! This package is currently under development! It is OK to test
but there are no warranties of any kind so far :-)

Install
=======

    devtools::install_bitbucket("cancercentrum/rccepi")
    library(rccepi)
    ?incidence
    ?incidence.test
